package gcp

import (
	"bufio"
	"cloud.google.com/go/storage"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

var client *storage.Client
var RetryableError = errors.New("upload: retryable error")

type JsonCredentials struct {
	TypeString              string `json:"type"`
	ProjectID               string `json:"project_id"`
	PrivateKeyID            string `json:"private_key_id"`
	PrivateKey              string `json:"private_key"`
	ClientEmail             string `json:"client_email"`
	ClientID                string `json:"client_id"`
	AuthURI                 string `json:"auth_uri"`
	TokenURI                string `json:"token_uri"`
	AuthProviderX509CertURI string `json:"auth_provider_x509_cert_url"`
	ClientX509CertURL       string `json:"client_x509_cert_url"`
}

// GetCredentials will retrieve the GCP credentials from machine.
func GetCredentials() JsonCredentials {
	path, ok := os.LookupEnv("GOOGLE_APPLICATION_CREDENTIALS")
	if !ok {
		log.Fatalf("Environment variable GOOGLE_APPLICATION_CREDENTIALS must be path to JSON credentials")
	}
	f, err := os.Open(path)
	if err != nil {
		log.Fatalf("No file found at: %s", path)
	}
	defer f.Close()
	byteValue, _ := ioutil.ReadAll(f)

	var result JsonCredentials
	err = json.Unmarshal([]byte(byteValue), &result)
	if err != nil {
		log.Fatalf("Could not marshal file: %s", path)
	}
	return result
}

// ValidateImageSize validate the user image size.
// If user try to upload too large image it will throw error.
func ValidateImageSize(ctx context.Context, obj *storage.ObjectHandle) error {
	attrs, err := obj.Attrs(ctx)
	if err != nil {
		return fmt.Errorf("upload: failed to get object attributes %q : %w",
			obj.ObjectName(), RetryableError)
	}
	// You can enlarge maximum size up to 20MB by modifying this line.
	if attrs.Size >= 1024*100 {
		return fmt.Errorf("upload: image file is too large, got = %d", attrs.Size)
	}
	// Validates obj and returns true if it conforms supported image formats.
	if err := validateMIMEType(ctx, attrs, obj); err != nil {
		return err
	}

	return nil
}

// validateMIMEType validated the image type.
func validateMIMEType(ctx context.Context, attrs *storage.ObjectAttrs, obj *storage.ObjectHandle) error {
	r, err := obj.NewReader(ctx)
	if err != nil {
		return fmt.Errorf("upload: failed to open new file %q : %w",
			obj.ObjectName(), RetryableError)
	}
	defer r.Close()

	if _, err := func(ct string) (image.Image, error) {
		switch ct {
		case "image/png":
			return png.Decode(r)
		case "image/jpeg", "image/jpg":
			return jpeg.Decode(r)
		default:
			return nil, fmt.Errorf("upload: unsupported MIME type, got = %q", ct)
		}
	}(attrs.ContentType); err != nil {
		return err
	}
	return nil
}

// IsBucketExists return true or false the bucket is exist or not.
func IsBucketExists(bh *storage.BucketHandle) bool {
	ctx := context.Background()
	_, err := bh.Attrs(ctx)
	exists := err == nil
	return exists
}

// ReaderFromFile read the file from os.
func ReaderFromFile(path string) (io.Reader, *os.File) {
	var r io.Reader
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	r = f
	return r, f
}

// CreateWithAttrs create attrs for the bucket.
func CreateWithAttrs(client *storage.Client, projectID, bucketName string) error {
	ctx := context.Background()
	bucket := client.Bucket(bucketName)
	if err := bucket.Create(ctx, projectID, &storage.BucketAttrs{
		Location: "US", // it should need to dynamic
	}); err != nil {
		return err
	}
	return nil
}

// UploadImage upload the user image file.
func UploadImage(bh *storage.BucketHandle, ctx context.Context, r io.Reader, name string, public bool) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	if _, err := bh.Attrs(ctx); err != nil {
		return nil, nil, err
	}

	reader := bufio.NewReader(r)
	content, _ := ioutil.ReadAll(reader)

	// Encode as base64.
	encoded := base64.StdEncoding.EncodeToString(content)

	// name must consist entirely of valid UTF-8-encoded runes
	obj := bh.Object(name)
	err := ValidateImageSize(ctx, obj)
	if err != nil {
		return nil, nil, RetryableError
	}
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, strings.NewReader(encoded)); err != nil {
		return nil, nil, err
	}
	if err := w.Close(); err != nil {
		return nil, nil, err
	}

	if public {
		if err := obj.ACL().Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
			return nil, nil, err
		}
	}

	attrs, err := obj.Attrs(ctx)
	return obj, attrs, err
}

func ObjectURL(objAttrs *storage.ObjectAttrs) string {
	return fmt.Sprintf("https://storage.googleapis.com/%s/%s", objAttrs.Bucket, objAttrs.Name)
}

func SignedURL(client *storage.Client, bucket, object string) error {
	pkey, err := ioutil.ReadFile("my-private-key.pem")
	if err != nil {
		return err
	}
	url, err := storage.SignedURL(bucket, object, &storage.SignedURLOptions{
		GoogleAccessID: "bucket-identity-staging@moneda-cacao.iam.gserviceaccount.com",
		PrivateKey:     pkey,
		Method:         "GET",
		Expires:        time.Now().Add(1 * time.Hour),
	})
	if err != nil {
		return err
	}
	fmt.Println(url)
	return nil
}