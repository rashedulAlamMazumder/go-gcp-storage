package gcp

import (
	"bytes"
	"cloud.google.com/go/storage"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
)

const key = "GOOGLE_APPLICATION_CREDENTIALS"

type fakeClient struct {
	Client
	buckets map[string]*fakeBucket
}

type fakeBucket struct {
	attrs   *storage.BucketAttrs
	objects map[string][]byte
}

func newFakeClient() Client {
	return &fakeClient{buckets: map[string]*fakeBucket{}}
}

func (c *fakeClient) Bucket(name string) BucketHandle {
	return fakeBucketHandle{c: c, name: name}
}

type fakeBucketHandle struct {
	BucketHandle
	c    *fakeClient
	name string
}

func (b fakeBucketHandle) Create(_ context.Context, _ string, attrs *storage.BucketAttrs) error {
	if _, ok := b.c.buckets[b.name]; ok {
		return fmt.Errorf("bucket %q already exists", b.name)
	}
	if attrs == nil {
		attrs = &storage.BucketAttrs{}
	}
	attrs.Name = b.name
	b.c.buckets[b.name] = &fakeBucket{attrs: attrs, objects: map[string][]byte{}}
	return nil
}

func (b fakeBucketHandle) Attrs(context.Context) (*storage.BucketAttrs, error) {
	bkt, ok := b.c.buckets[b.name]
	if !ok {
		return nil, fmt.Errorf("bucket %q does not exist", b.name)
	}
	return bkt.attrs, nil
}

func (b fakeBucketHandle) Object(name string) ObjectHandle {
	return fakeObjectHandle{c: b.c, bucketName: b.name, name: name}
}

type fakeObjectHandle struct {
	ObjectHandle
	c          *fakeClient
	bucketName string
	name       string
}

func (o fakeObjectHandle) NewReader(context.Context) (Reader, error) {
	bkt, ok := o.c.buckets[o.bucketName]
	if !ok {
		return nil, fmt.Errorf("bucket %q not found", o.bucketName)
	}
	contents, ok := bkt.objects[o.name]
	if !ok {
		return nil, fmt.Errorf("object %q not found in bucket %q", o.name, o.bucketName)
	}
	return fakeReader{r: bytes.NewReader(contents)}, nil
}

func (o fakeObjectHandle) Delete(context.Context) error {
	bkt, ok := o.c.buckets[o.bucketName]
	if !ok {
		return fmt.Errorf("bucket %q not found", o.bucketName)
	}
	delete(bkt.objects, o.name)
	return nil
}

type fakeReader struct {
	Reader
	r *bytes.Reader
}

func (r fakeReader) Read(buf []byte) (int, error) {
	return r.r.Read(buf)
}

func (r fakeReader) Close() error {
	return nil
}

func (o fakeObjectHandle) NewWriter(context.Context) Writer {
	return &fakeWriter{obj: o}
}

type fakeWriter struct {
	Writer
	obj fakeObjectHandle
	buf bytes.Buffer
}

func (w *fakeWriter) Write(data []byte) (int, error) {
	return w.buf.Write(data)
}

func (w *fakeWriter) Close() error {
	bkt, ok := w.obj.c.buckets[w.obj.bucketName]
	if !ok {
		return fmt.Errorf("bucket %q not found", w.obj.bucketName)
	}
	bkt.objects[w.obj.name] = w.buf.Bytes()
	return nil
}
func TestGetCredentialsSuccessfully(t *testing.T) {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	keyFile := dir + string(os.PathSeparator) + "keys_test.json"
	_ = os.Setenv(key, keyFile)
	defer func() {
		_ = os.Unsetenv(key)
		_ = os.Remove(keyFile)
	}()

	j := JsonCredentials{ProjectID: "Test ID"}
	b, _ := json.MarshalIndent(j, "", " ")
	_ = ioutil.WriteFile("keys_test.json", b, 0644)

	c := GetCredentials()
	if c.ProjectID != j.ProjectID {
		t.Error("Got:", c.ProjectID, "| Want:", j.ProjectID)
	}
}

func TestValidateImageSize(t *testing.T) {
	//TODO
}

func TestCreateWithAttrs(t *testing.T) {
	_ = fakeClient{}.Client
	err := CreateWithAttrs(client, "Test ID", "Test Bucket")
	if err != nil {
		t.Error("Got:", err, "| Want:", nil)
	}
}

func TestValidateMIMETypeShouldFailToOpenFile(t *testing.T) {
	ctx := context.TODO()
	obj := storage.ObjectHandle{}
	bucket := storage.BucketHandle{}
	_ = bucket.Object("apple.txt").NewWriter(ctx)
	attrs, _ := obj.Attrs(ctx)

	err := validateMIMEType(ctx, attrs, &obj)
	if !strings.Contains(`upload: failed to open new file "" : upload: retryable error`, err.Error()) {
		t.Error("Got:", err.Error(), "| Want: 'upload: failed to open new file...'")
	}
}

func TestIsBucketDoesNotExist(t *testing.T) {
	bucket := storage.BucketHandle{}
	bucket.Attrs(context.TODO())
	exists := IsBucketExists(&bucket)
	if exists == true {
		t.Error("Got:", exists, "| Want: false")
	}
}